from . import views
from django.urls import path

from .views import *

urlpatterns = [
    path('/read_blog/', postinfo),
    path('/create_blog/', postinfo),
    path('/delete_blog/', postinfo),
    path('/update_blog/', postinfo),
]

'''

    path('/read_blog/<slug:pk_slug>', Post_with_param),
'''