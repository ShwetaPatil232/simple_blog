# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Post

# Register your models here.
#admin.site.register(Post)


class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title','author', 'created_on', 'updated_on', 'content', 'status' )
    list_filter = ("status",)
    search_fields = ['title', 'content']


admin.site.register(Post, PostAdmin)


