import json
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.db import models
from django.views.decorators.http import require_http_methods
from .models import Post

@require_http_methods(['POST','GET','PATCH','DELETE'])
def postinfo(request):
    if request.method == 'POST':
        param = json.loads(request.body)
        if not param.get('title'):
            return JsonResponse({
                "message" : "Field missing",
                "status" : False
            })
        instance = Post.objects.create(**param)
        return JsonResponse({
            "message" : "success",
            "status" : True ,
            "data" : [instance.get_json()]
        })


    if request.method == 'GET':
        data = []
        queryset = Post.objects.all()
        for instance in queryset:
            data.append(instance.get_json())

        return JsonResponse({
            "message": "success",
            "status": True,
            "data": data
        })



    if request.method == 'DELETE':
        param = json.loads(request.body)
        if not param.get('title'):
            return JsonResponse({
                "message": "Title Field missing" ,
                "status": False
            })
        Post.objects.get(title = param['title']).delete()
        return JsonResponse({
            "message": "Post deleted successfully",
            "status": True
        })


    if request.method == 'PATCH':
        param = json.loads(request.body)
        if not param.get('title'):
            return JsonResponse({
                "message": "Field missing" ,
                "status": False
            })
        if not param.get('new_title'):
            return JsonResponse({
                "message": "New Title Field missing" ,
                "status": False
            })

        Post.objects.filter(title=param['title']).update(title=param['new_title'],
                                                         author=param['author'],
                                                         content=param['content'],
                                                         status=param['status'])
        instance = Post.objects.get(title=param['new_title'])

        return JsonResponse({
            "message": "Workspace Updated successfully",
            "status": True ,
            "data": [instance.get_json()]
        })

'''
def Post_with_param(request, pk_slug):
    if pk_slug is not None and pk_slug.isdigit():
        instance = Post.objects.get(id=pk_slug)
        if instance.id == pk_slug:
            msg = f"Workspace with id {pk_slug}"
            return JsonResponse({
                    "message": msg,
                    "status": True,
                    "data": [instance.get_json()]
            })
    elif pk_slug is not None:
        instance = Post.objects.get(title=pk_slug)
        if instance.title == pk_slug:
            msg = f"Workspace with Title {pk_slug}"
            return JsonResponse({
                    "message": msg,
                    "status": True,
                    "data": [instance.get_json()]
            })
    else:
        return JsonResponse({
                "message": "Data Does Not Found.",
                "status": False,
        })
'''